# BandTShits App

This consists of two parts:
* Smart Contracts written in Solidity
** Run under Hardhat for devlopment and deployment
* Web App to interface with smart contracts

## Web App
Based on React dApp Template (Vite) - https://github.com/huseyindeniz/vite-react-dapp-template

![version](https://img.shields.io/github/package-json/version/huseyindeniz/vite-react-dapp-template)
![build](https://img.shields.io/github/actions/workflow/status/huseyindeniz/vite-react-dapp-template/CI.yml)

A Vite React template specifically designed for decentralized application (dApp) frontend development.


## Smart Contracts and Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, and a script that deploys that contract.

Try running some of the following tasks:

```shell
npx hardhat help
npx hardhat test
REPORT_GAS=true npx hardhat test
npx hardhat node
npx hardhat run scripts/deploy.ts
```
