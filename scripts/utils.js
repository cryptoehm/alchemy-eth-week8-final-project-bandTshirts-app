const { ethers } = require("hardhat")
const abi = require("../artifacts/contracts/BandTShirts.sol/BandTShirts.json")
require("dotenv").config();

const bandTShirtsAddress=process.env.BANDTSHIRTS_CONTRACT
const eventAddress=process.env.EVENT_CONTRACT

const getContracts = async() => {
    if (!(bandTShirtsAddress && eventAddress)) {
        throw Error("Need to define BANDTSHIRTS_CONTRACT and EVENT_CONTRACT in .env")
    }
    const signers = await ethers.getSigners();
    const owner = signers[0];

    const bandTShirtsContract = await ethers.getContractAt("BandTShirts", bandTShirtsAddress, owner);
    const eventContract = await ethers.getContractAt("BandEvent", eventAddress, owner);

    console.log(`Utils - owner: ${await owner.getAddress()}`)
    if (!bandTShirtsContract.getAddress()) {
        throw new Error("Cannot get contracts for bandTShirts");
    }
    return { bandTShirtsContract, bandTShirtsAddress, eventContract, eventAddress, signers };
}

/** Needed for event decoding **/
const getContractFromABI = async() => {
    if (!process.env.BANDTSHIRTS_CONTRACT) {
        throw Error("Need to define BANDTSHIRTS_CONTRACT in .env")
    }
    const eventABI = abi.abi.filter(a => a.type == "event" && a.name == "BandEventCreated");
    console.log(`event abi: ${JSON.stringify(eventABI)}`)
    const bandTShirtsContract = new ethers.Contract(bandTShirtsAddress, eventABI);
    return { bandTShirtsContract, bandTShirtsAddress };
}
const getNFTMetadata = async() => {
    if (!process.env.NFT_METADATA_IPFS) {
        throw Error("NFT_METADATA_IPFS not found in .env")
    }
    return process.env.NFT_METADATA_IPFS;
}

module.exports = { getContracts, getContractFromABI, getNFTMetadata }
