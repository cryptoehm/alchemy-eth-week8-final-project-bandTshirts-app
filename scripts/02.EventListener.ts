const { getContracts: getContractsHere, getContractFromABI } = require("./utils")

async function handleEvents() {
  const {bandTShirtsContract, bandTShirtsAddress } = await getContractsHere();
  console.log(`listen for BandEventCreated via BandContract: ${bandTShirtsAddress}`)
  let eventLog;

  // When created this way it returns only a single value AND it stops the other event handler from returning full data
  // const tx = await bandTShirtsContract.on('BandEventCreated', (args: any[]) => {
  //   console.log(`event BandEventCreated w args: ${JSON.stringify(args, null, 2)}, length: ${args.length}`)
  // });
  const decodeEvent = async (eventLog: any) => {
    const {bandTShirtsContract:c, bandTShirtsAddress: a} = await getContractFromABI();
    console.log(`listen for BandEventCreated via BandContract: ${a}`)

    const decodedTopics = c.interface.decodeEventLog(
        "BandEventCreated",
        eventLog.data,
        eventLog.topics
    );

    console.log("Decoded Topics:", decodedTopics);
  }

  const eventHandler = (a: any) => {
    console.log(`Event:`, a.log);
    eventLog = a.log;
    decodeEvent(eventLog).then(() => console.log(`decodeEvent done`));
  };

  // Create a filter to listen for the specific event
  const eventFilter = bandTShirtsContract.filters['BandEventCreated']();

  // Start the event listener
  bandTShirtsContract.on(eventFilter, eventHandler);

  console.log(`after event handler setup`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
handleEvents().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
