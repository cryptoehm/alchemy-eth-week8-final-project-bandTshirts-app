const hre = require("hardhat");
const { ethers } = hre;

async function verify(contractAddress: string) {
  const provider = hre.network.name;
  if (!provider || provider == "localhost") {
    console.log(`Not verifying - provider: ${provider}`);
    return;
  }
  try {
    await hre.run("verify:verify", {
      address: contractAddress,
      constructorArguments: [
      ]
    })
  } catch (e: any) {
    if (e.message.toLowerCase().includes("already verified")) {
      console.log("Already verified!")
    } else {
      console.error(e.message)
    }
  }
}

async function deployBandTShirts() {
  const bandTShirts = await ethers.deployContract("BandTShirts");

  await bandTShirts.waitForDeployment();
  console.log(
      `BandTShirts deployed to ${bandTShirts.target}`
  );
  await verify(await bandTShirts.getAddress());
}

async function deploy() {
  await deployBandTShirts();
  // Contracts imported into the 'top-level' one are deployed as part of it
  // await deployBandEvents();
  // await deployMerchNFT();
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
deploy().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
