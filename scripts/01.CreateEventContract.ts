const { getContracts: getContracts2 } = require("./utils")

async function main() {
  const oneMonthSecs = 31 * 24 * 3600;
  const eDate = Math.trunc(Date.now() / 1000) + oneMonthSecs;  // seconds
  const eName = "The Beatles Reunion"

  const {bandTShirtsContract, bandTShirtsAddress } = await getContracts2();
  console.log(`CreateEvent contract with date: ${eDate} and name: ${eName} via BandContract: ${bandTShirtsAddress}`)

  const tx = await bandTShirtsContract.createEventsContract(eDate, eName);
  const receipt = await tx.wait();
  const address = receipt.logs[0].address;
  // const events = receipt.events;
  console.log(`Created btsEvent - address: ${address}`)
  console.log(`receipt: ${JSON.stringify(receipt, null, 2)}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
