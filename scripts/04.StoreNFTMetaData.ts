const {NFTStorage} = require('nft.storage')
const {getContracts} = require("./utils")

const API_KEY = process.env.NFT_STORAGE_API_KEY

// For example's sake, we'll fetch an image from an HTTP URL.
// In most cases, you'll want to use files provided by a user instead.
async function getExampleImage() {
    const imageOriginUrl = "https://user-images.githubusercontent.com/87873179/144324736-3f09a98e-f5aa-4199-a874-13583bf31951.jpg"
    const r = await fetch(imageOriginUrl)
    if (!r.ok) {
        throw new Error(`error fetching image: [${r.statusText}]: ${r.status}`)
    }
    return r.blob()
}

async function storeExampleNFT() {
    const image = await getExampleImage()
    const nft = {
        image, // use image Blob as `image` field
        name: "MerchDC",
        symbol: "MDC",
        description: "The metaverse is here. Where is it all being stored?",
        youtube_url: "https://www.youtube.com/watch?v=Q8Bv_CBeSZs",
        attributes: [
            {
                display_type: "date",
                "trait_type": "birthday",
                value: 1546360800
            },
            {
                "trait_type": "Personality",
                value: "Sad"
            }
        ]
    }

    const client = new NFTStorage({token: API_KEY})
    const metadata = await client.store(nft)

    console.log('NFT data stored!')
    console.log('Metadata URI: ', metadata.url)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
storeExampleNFT().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
