const { getNFTMetadata, getContracts: getContracts3 } = require("./utils")

async function createMerchNFT() {
  const oneMonthSecs = 31 * 24 * 3600;
  const eDate = Math.trunc(Date.now() / 1000) + oneMonthSecs;  // seconds
  const eName = "The Beatles Reunion"

  const {eventContract, eventAddress, signers } = await getContracts3();
  console.log(`Create MerchNFT via eventContract: ${eventAddress}`)

  const tx = await eventContract.createNewMerchNFTs(signers[1], getNFTMetadata());
  const receipt = await tx.wait();
  console.log(`receipt: ${JSON.stringify(receipt, null, 2)}`)
  const address = receipt.logs[0].address;
  // const events = receipt.events;
  console.log(`Created MerchNFT - address: ${address}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
createMerchNFT().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
