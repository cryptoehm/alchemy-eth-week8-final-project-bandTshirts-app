import {HardhatUserConfig} from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "@nomicfoundation/hardhat-verify";

require("dotenv").config();

const config: HardhatUserConfig = {
    defaultNetwork: 'localhost',
    solidity: "0.8.23",
    sourcify: {
        enabled: true
    },
    networks: {
    },
    etherscan: {
        // Your API key for Etherscan
        // https://docs.blockscout.com/for-users/verifying-a-smart-contract/hardhat-verification-plugin says arbitrary ok
        apiKey: "abc"
    },
    paths: {
        // Deployments require access to the artifacts
        artifacts: "./src/artifacts"
    }
};

export default config;
