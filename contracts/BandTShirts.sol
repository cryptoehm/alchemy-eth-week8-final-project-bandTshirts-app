// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.23;
import "hardhat/console.sol";

/*
 * @author - bbos.eth
 * Initializable - I won't use Proxies for now so as to keep this simpler - https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies
 *      However it is a future modification and I will structure this ready for such a change.
 * Ownable - It's nice to use well defined and tested code.
 */
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import "./BandEvent.sol";

contract BandTShirts is Initializable, Ownable {
    BandEvent[] private bandEvents;

    event BandEventCreated(address indexed eventAddress, uint indexed eventDate, string eventName);

    constructor() Ownable(msg.sender){
//        initialize();
    }

    function initialize() public onlyInitializing {
        // Maybe should use proxies, but KISS for now
        // https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies
    }

    function _addEvent(BandEvent _event) private {
        bandEvents.push(_event);
    }

    function getEvents() public view returns(BandEvent[] memory) {
        return bandEvents;
    }

    // http://localhost:4321/developer/userstories/#seller-purchases-smart-contract-from-bandtshirts-inc
    function createEventsContract(uint _eventDate, string memory _eventName) public onlyOwner returns (address){
        BandEvent newEvent = new BandEvent(msg.sender, _eventDate, _eventName);
        bandEvents.push(newEvent);
        emit BandEventCreated(address(newEvent), _eventDate, _eventName);
        console.log('createEventsContract - name: %s, date: %s, address: ', _eventName, _eventDate, address(newEvent));
        return address(newEvent);
    }

    // Call getEvents()
    //    function ecosystemOverview() {}
}
