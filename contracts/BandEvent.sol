// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.23;

import "hardhat/console.sol";

/*
 * @author - bbos.eth
 * Initializable - I won't use Proxies for now so as to keep this simpler - https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies
 *      However it is a future modification and I will structure this ready for such a change.
 * Ownable - It's nice to use well defined and tested code.
 */
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import "./Types.sol";
import {MerchNFT} from "./MerchNFT.sol";

contract BandEvent is Initializable, Ownable {
    uint256 private eventDate;
    string private eventName;

    // Map _to to (_eventDate, _eventName) to [merchNFTs there]
    mapping(address => EventData) events;

    event createNewMerchNFT(address indexed _to, string _metadataUri, address merchNFTAddress);

    constructor(address _creator, uint _eventDate, string memory _eventName) Ownable(_creator){
        eventDate=_eventDate;
        eventName=_eventName;
        //        initialize();
//        events[msg.sender] = EventData(_eventDate, _eventName);
    }

    function initialize(address owner) public onlyInitializing {
    }

    function createNewMerchNFTs(address _to, string memory _metadataUri) public onlyOwner returns (address) {
        MerchNFT merchNFT = new MerchNFT();
        merchNFT.mintMerchNFT(_to, _metadataUri);
//        events[_to]
        emit createNewMerchNFT(_to, _metadataUri, address(merchNFT));
        console.log('createNewMerchNFTs - to: %s, metadataUri: %s, address: %s', _to, _metadataUri, address(merchNFT));
        return address(merchNFT);
    }
}
