// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.23;

import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155URIStorage.sol";
import "hardhat/console.sol";

contract MerchNFT is ERC1155 {
    uint256 private _tokenIdCounter;
    mapping (uint256 => string) private _tokenURIs;

    event mint(address indexed _to, uint256 _tokenId, string _metadataUri);

    constructor() ERC1155("The uri will be set for each NFT") {
    }

    function uri(uint256 tokenId) override public view returns (string memory) {
        return(_tokenURIs[tokenId]);
    }

    function _setTokenUri(uint256 tokenId, string memory tokenURI) private {
        _tokenURIs[tokenId] = tokenURI;
    }

    function mintMerchNFT(address _to, string memory _metadataUri) public {
        uint256 tokenId = _tokenIdCounter;
        _mint(_to, tokenId, 1, "");
        _setTokenUri(tokenId, _metadataUri);
        console.log("MintMerchNFT - to: %s, metadataURI: %s, tokenId: %s", _to, _metadataUri, tokenId);

        emit mint(_to, tokenId, _metadataUri);

        _tokenIdCounter += 1;
    }
}
