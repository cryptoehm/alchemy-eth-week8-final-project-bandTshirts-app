// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.23;

struct EventData {
    // Number of seconds since the Epoch
    uint eventDate;
    string eventName;
}
